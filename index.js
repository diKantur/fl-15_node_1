const express = require("express");
const app = express();
const fs = require("fs");
const path = require("path");
const morgan = require("morgan");

const filesPath = path.dirname("./") + "/api/files/";

app.use(morgan("tiny"));
app.use(express.json());
app.get('/', () => res.end())
app.post("/api/files", (req, res) => createFile(req, res));
app.get("/api/files", (req, res) => getFiles(req, res));
app.get("/api/files/:filename", (req, res) => getFile(req, res));

app.use((req, res) => res.status(500).json({ message: "Server error" }));

app.listen(8080);

function checkExtension(ext) {
  return /.(log|txt|json|yaml|xml|js)$/g.test(ext);
}

function createFile(req, res) {
  try {
    if (req.body.content && checkExtension(path.extname(req.body.filename))) {
      fs.writeFile(filesPath + req.body.filename, req.body.content, () => res.json({ message: "File created successfully" }));
    } else {
      throw new Error()
    }
  } catch (err) {
    res.status(400).json({ message: "Please specify 'content' parameter" })
  }
}

function getFiles(req, res) {
  try {
    fs.readdir(filesPath, (err, files) => res.json({ message: "Successful", files: files.filter(v => v !== "README.md") }))
  } catch (err) {
    res.status(400).json({ message: "Client error" })
  }
}

function getFile(req, res) {
  try {
    let result = {
      message: "Success",
      filename: "",
      content: "",
      extension: "",
      uploadedDate: "",
    };

    result.uploadedDate = fs.statSync(filesPath + req.params.filename).birthtime

    result.filename = req.params.filename;
    result.extension = path.extname(filesPath + req.params.filename).replace(".", "");

    fs.readFile(filesPath + req.params.filename, "utf8", (err, file) => {
      result.content = file
      res.json(result)
    })
  } catch (err) {
    res.status(400).json({ message: `No file with '${req.params.filename}' filename found` })
  }
}